   2/2/2011                                   Southside Classic 2010
Powerlifting Results                 Bwt    WtCls   (1) PL      (2)      (3)       Age- Pl                        USAPL
                       Team   Div                                                             Pl-Div- WtCls-Evt
Name                                 (kg)    (kg)    Total   Wilks Pts         Wilks   Code                       Number
Meg Susong                    F-JR  43.3     44      255     362.890       370.148      1       1-F-JR-44-PL      14315
April Guzman                  F-T   46.4     48      230     312.179       352.762      1       1-F-T-48-PL
Bryanna Ybarra                F-T   46.4     48      210     285.033       307.836      1       2-F-T-48-PL
Cameron Ruiz                  F-T  50.01     52     177.5    227.981       269.018      1       1-F-T-52-PL
Laura McGill           SMAC   F-T   55.2     56     297.5    354.025       368.186      1       1-F-T-56-PL       12066
Angela Rodriguez              F-T   53.3     56     247.5    302.693       357.177      1       2-F-T-56-PL       91609
Monica Moore                  F-T   54.3     56      215     139.470                    1       3-F-T-56-PL
Candice Ornelas               F-T  52.02     56     182.5    227.450       257.018      1       4-F-T-56-PL
Amanda Eckols                 F-T   62.7    67.5    342.5    369.181       398.715      1       1-F-T-67.5-PL
Alma Rodriguez                F-T   64.7    67.5      0       0.000         0.000       1                         93705
Natalie Escareno              F-JR  71.2     75      260     255.684       263.355      1       1-F-JR-75-PL
Michelle Arce                 F-T   74.9     75       0       0.000         0.000       1
Tiffany McKinney              F-JR  85.9     90     462.5    407.971       416.131      1       1-F-JR-90-PL       1933
Saby Santos                   F-T    90      90     287.5    248.429       268.303      1        1-F-T-90-PL
Clarissa Cervantez            F-T   116     90+     372.5    299.714       317.696      1       1-F-T-90+-PL      13621
Reece Walker                  M-T    56      56       0       0.000         0.000       1
Brandon Jones                 M-T   54.2     56     352.5    346.648                    1       1-M-T-56-PL
Abel Escamilla                M-T   63.9    67.5    567.5    457.802       476.114      1      1-M-T-67.5-PL      12900
Zane McCoy             SMAC   M-JR  72.7     75      365     265.903       273.880      1      1-M-JR-75-PL
Shawn Frasquillo       SMAC   M-O   72.9     75      595     432.625        0.000       1       1-M-O-75-PL        6487
Tim Lamando                   M-T  74.05     75      555     531.856        0.000       1        1-F-T-75-PL
Richard Pena                  M-T   81.4    82.5    522.5    352.897       398.773      1      1-M-T-82.5-PL
Ramiro Espinoza               M-JR 84.85     90      590     388.810       392.698      1      1-M-JR-90-PL
Jerry saldana                 M-O  88.55     90       0       0.000         0.000       1                         15271
Dylan Thomas                  MR-O 87.95     90      565     365.046       365.046      1       1-MR-O-90-PL      15280
Francisco Cisneros            M-T   89.1     90       0       0.000         0.000       1
Justin Garcia                 M-T  87.95     90      670     432.887       458.860      1        2-M-T-90-PL      19860
Ian Bell                      M-T   87.3     90      818     530.637       562.475      1        1-M-T-90-PL       2716
Chuck Akers                   M-O   99.2    100     557.5    340.409       390.450      1       1-M-O-100-PL       3715
Eric Lopez                    M-JR 104.9    110       0       0.000         0.000       1
Jorge Pillado                 M-JR  109     110      810     478.062       478.062      1      1-M-JR-110-PL
Ernest Espinosa Jr            M-M 109.5     110      495     291.703       311.539      1      1-M-M-110-PL
J.J. Natal             SMAC   MR-O 106.5    110     557.5    331.490       331.490      1      1-MR-O-110-PL      10179
Rob Garza                     M-T 109.85    110     612.5    360.579       382.213      1       2-M-T-110-PL
Brandon Brashear              M-T  109.4    110     692.5    408.229       432.722      1       1-M-T-110-PL       9918
Andre Gholson         Bell   M-O    122.9    125       0        0.000         0.000       1
Sean Berry                   MR-O   124.7    125     662.5     377.691        0.000       1      1-MR-O-125-PL      18265
Kirill Olenych               M-T    121.9    125      600      343.740       357.490      1       1-M-T-125-PL
Arron Gonzales               M-JR   136.8   125+      855      479.484        0.000       1      1-M-JR-125+-PL     14382

Bench Press Results                 Bwt     WtCls   (1) Best      (2)      (3)       Age- Pl                        USAPL
                      Team   Div                                                                Pl-Div- WtCls-Evt
Name                                (kg)     (kg)      BP      Wilks Pts         Wilks   Code                       Number
Ted Edwards                  MR-O    72.3     75       70      51.205         93.961      1      1-MR-O-75-BP
Gerry Garza                  M-M     72.5     75      150      109.500       115.523      1      1-M-M-75-BP        17600
Gerry Garza                  M-O     72.5     75      150      109.500       115.523      1       1-M-O-75-BP       17600
Jerry saldana                M-O    88.55     90       0        0.000         0.000       1                         15271
Andre Gholson                M-O    122.9    125       0        0.000         0.000       1
Ryan Carrillo         Bell   M-T     141    125+       0        0.000         0.000       1                         16272
