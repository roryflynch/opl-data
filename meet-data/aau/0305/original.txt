                         Virginia Powerlifting Association
                         Final Report of Events
                         Male Lifters
2003 Fire/Law Nationals-Assisted                                                                 * Weights listed in kilograms
Wt Cl Name                 State   Age   Weight                Catagory                 Squat      4th   Bench    4th   Deadlift   4th   Total Weight
242   Kenneth James        VA      41    242      Law/Fire/Military / Masters (40-44)   235.00           145.00         277.50             657.50
242   Charles Swingler     VA      36    232      Law/Fire/Military / Open              255.00           182.50         255.00             692.50
242   Charles Swingler     VA      36    232      Law/Fire/Military / Sub Masters (35-3 255.00           182.50         255.00             692.50
319   Paul Bates           VA      40    290      Masters (40-44)                       327.50           187.50         277.50             792.50
319   Paul Bates           VA      40    290      Open                                  327.50           187.50         277.50             792.50
                      Virginia Powerlifting Association
                      Final Report of Events
                      Male Lifters
2003 Fire/Law Nationals-Raw                                                                      * Weights listed in kilograms
Wt Cl Name              State   Age   Weight                 Catagory                   Squat      4th   Bench    4th   Deadlift   4th   Total Weight
220   Timothy Brien     RI      44    216      Law/Fire/Military / Masters (40-44)      260.00           150.00         260.00             670.00
220   Timothy Brien     RI      44    216      Lifetime/Law/Fire/Military / Masters (   260.00           150.00         260.00             670.00
220   Timothy Brien     RI      44    216      Masters (40-44)                          260.00           150.00         260.00             670.00
                       Virginia Powerlifting Association
                       Final Report of Events
                       Male Lifters
2003 Spring Break Bench-Assisted                                                          * Weights listed in kilograms
Wt Cl Name                    State   Age   Weight                Catagory        Squat     4th   Bench    4th   Deadlift   4th   Total Weight
165   Chris Balance (bench)           16    156      Teenage (16-17)              0.00            100.00          0.00              100.00
181   Jason Metz (bench)      NC      23    176      Junior (20-23)               0.00            147.50          0.00              147.50
181   Jason Metz (bench)      NC      23    176      Open                         0.00            147.50          0.00              147.50
242   Nathan Bryant (bench)   VA      45    233      Lifetime / Masters (45-49)   0.00            117.50          0.00              117.50
